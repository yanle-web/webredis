# webredis

## 介绍
基于websocket+redis+nodejs实现web端消息推送

webredis是一个在node.js下运行的web服务，用于支持web端实现redis消息订阅发布。

## 安装

	cd webredis
	# 安装依赖库
	npm install
	# 或cnpm install


## 使用

系统要求： node.js

### 运行node.js 服务
	
	cd webredis
	node webredis.js

显示帮助信息`node webredis.js -h`

	Usage: node.js webredis.js [OPTION...]
	
	选项：
	  --help, -h  show the messae                                             [布尔]
	
	  --version   显示版本号                                                  [布尔]
	
	  --port, -p  webredis web port                                 [默认值: "16379"]
	
	  --rhost     redis host                                   [默认值: "localhost"]
	
	  --rport     redis port                                          [默认值: 6379]
	
	  --rauth     redis password                                        [默认值: ""]
	
	  --rdb       redis database                                         [默认值: 0]

### 运行演示页面

1. 进入test文件夹,打开index.html。
2. 可以再网页进行一系列redis的操作:订阅、取消订阅、发送信息


## 服务端redis请求接口(event)

### subscribe (订阅频道)

|参数|类型|说明|
|:-|:-|:-|
|channels|string or string[] |频道名，或频道列表|

### unsubscribe (取消订阅)

|参数|类型|说明|
|:-|:-|:-|
|channels|string or string[] |频道名，或频道列表|


### pubish (发布消息)

|参数|类型|说明|
|:-|:-|:-|
|channel|string |目标频道名|
|message|string |要发布的消息|


## 服务端推送接口

### message (订阅消息)

|参数|类型|说明|
|:-|:-|:-|
|message|json |收到的消息|

message消息字段说明

|参数|类型|说明|
|:-|:-|:-|
|channel|string |订阅频道名|
|data|string|收到的消息数据|

### ack (响应消息)

|参数|类型|说明|
|:-|:-|:-|
|ack|json |服务端redis请求接口的返回响应消息|

ack消息字段说明

|参数|类型|说明|
|:-|:-|:-|
|success|boolean |redis调用请求是否成功|
|request|string |redis调用请求名,such as 'publish','subscribe','unsubscribe'|
|reply|any|redis调用的返回值,类型由redis调用的返回值决定|
|respone|string|收到的响应数据|
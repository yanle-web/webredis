/**
 * 运行配置参数
 * @author guyadong
 */
const yargs = require('yargs');
const sprintf = require('sprintf-js').sprintf
// 命令参数定义
let argv = yargs
    .option('help' ,{alias : 'h',boolean : true, describe : 'show the messae' })
    .option('port' ,{alias : 'p',default : '16379', describe : 'webredis web port'})
    .option('rhost',{default : 'localhost',describe : 'redis host'})
    .option('rport',{default : 6379, describe : 'redis port'})
    .option('rauth',{default : '',describe : 'redis password'})
    .option('rdb'  ,{default : 0,describe : 'redis database'})
    .option('rurl' ,{describe : 'redis url,ingnore other redis params if present'})
    .usage('Usage: node.js webredis.js [OPTION...] ')
    .argv;

if(argv.help){
  // 输出帮助信息后退出
  yargs.help()
  .exit()
}
// node.js服务端口
var PORT = argv.port;
// redis 访问地址
if(argv.rurl){
  var REDIS_URL = argv.rurl
}else{
  var REDIS_URL = sprintf('redis://%s%s:%s/%d',argv.rauth.length==0?'':':'+argv.rauth +'@', argv.rhost , argv.rport ,argv.rdb)
}
module.exports.PORT = PORT
module.exports.REDIS_URL = REDIS_URL